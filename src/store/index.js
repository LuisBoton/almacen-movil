import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)
import warehouse from './modules/warehouse'
import categoria from './modules/categoria'
import productos from './modules/productos'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      warehouse,
      productos,
      categoria
    },
    states: {},
    actions: {
    },
    mutations: {
    },
    getters: {
    },
    strict: process.env.DEBUGGING
  })

  return Store
}
