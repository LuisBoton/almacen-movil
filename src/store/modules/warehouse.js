import axios from 'axios'
export default {
  namespaced: true,
  state: {
    depositos: [
    ],
    depositosfiltrados: []
  },
  actions: {
    traerdepositos ({ commit }) {
      axios.get('http://nacho.ninja/take/server/api/warehouses')
        .then((response) => {
          commit('modificardepositos', response.data)
        })
    },
    traerELdeposito (state, number) {
      state.depositosfiltrados = state.depositos.filter((respuesta) => respuesta.warehouse_id(number) === parseInt(number))
    },
    agregardeposito (datos) {
      axios.post('http://nacho.ninja/take/server/api/warehouses', datos)
        .then((response) => {
          console.log(response)
        })
        .catch(console.log('No se pudo crear deposito'))
    }
  },
  mutations: {
    modificardepositos (state, datos) {
      state.depositos = datos
    }
  }
}
