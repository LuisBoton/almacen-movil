import axios from 'axios'
export default {
  namespaced: true,
  state: {
    todosproductos: []
  },
  actions: {
    traerproductos ({ commit }) {
      axios.get('http://nacho.ninja/take/server/api/products/')
        .then((response) => {
          commit('todoslosproductos', response.data)
        })
    }
  },
  mutations: {
    todoslosproductos (state, datos) {
      state.todosproductos = datos
    }
  }
}
