import axios from 'axios'
export default {
  namespaced: true,
  state: {
    categorias: []
  },
  actions: {
    traercategorias ({ commit }) {
      axios.get('http://nacho.ninja/take/server/api/categories')
        .then((response) => {
          commit('todaslascategorias', response.data)
        })
    }
  },
  mutations: {
    todaslascategorias (state, datos) {
      state.categorias = datos
    }
  }
}
