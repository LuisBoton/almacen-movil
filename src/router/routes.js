
const routes = [
  {
    path: '/',
    component: () => import('layouts/LayOutLogin.vue'),
    children: [
      { path: '', component: () => import('pages/login.vue') }
    ]
  },
  {
    path: '/home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'home', component: () => import('pages/Index.vue') },
      { path: 'xxx', name: 'xxx', component: () => import('pages/xxx.vue') },
      { path: 'categorias', name: 'categorias', component: () => import('pages/categorias.vue') },
      { path: 'about', name: 'about', component: () => import('pages/about.vue') },
      { path: 'deposito/:id/:nombre', name: 'deposito', component: () => import('pages/deposito.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
